package sbu.cs;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.Buffer;
import java.util.Scanner;

public class Server {

    /**
     * Create a server here and wait for connection to establish,
     *  then get file name and after that get file and save it in the given directory
     *
     * @param args an string array with one element
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        // below is the name of directory which you must save the file in it
        //String directory = args[0];     // default: "server-database"
        ServerSocket serverSocket = new ServerSocket(2727);
        Socket socket = serverSocket.accept();
        System.out.println("Conncecion Established!");
        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
        DataInputStream inputStream = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
        String fileName = inputStream.readUTF();
        int fileSize = inputStream.readInt();
        System.out.println("File name: " + fileName);
        System.out.println("File size: " + fileSize);
        byte[] fileContent = new byte[fileSize];
        inputStream.readFully(fileContent);
        File fileObj = new File(args[0]+'/'+fileName);
        FileOutputStream fileOutputStream = new FileOutputStream(fileObj);
        fileOutputStream.write(fileContent);
        String validation = "";
        if (fileSize == (int)fileObj.length())
        {
            validation = "Done";
            dataOutputStream.writeUTF(validation);
        }
        else
        {
            while (fileSize != (int)fileObj.length())
            {
                validation = "Not Done";
                dataOutputStream.writeUTF(validation);
                inputStream.readFully(fileContent);
                fileOutputStream.write(fileContent);
            }
            validation = "Done";
            dataOutputStream.writeUTF(validation);
        }
        fileOutputStream.close();
        dataOutputStream.close();
        inputStream.close();
        serverSocket.close();
        socket.close();
    }
}
