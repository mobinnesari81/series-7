package sbu.cs;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    /**
     * Create a socket and connect to server then send fileName and after that send file to be saved on server side.
     *  in here filePath == fileName
     *
     * @param args a string array with one element
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        //String filePath = args[0];      // "sbu.png" or "book.pdf"
        Socket socket = new Socket("localhost",2727);
        System.out.println("Connection established!");
        DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
        File fileObj = new File(args[0]);
        String line = fileObj.getName();
        dataOutputStream.writeUTF(line);
        dataOutputStream.flush();
        int fileSize = (int) fileObj.length();
        dataOutputStream.writeInt(fileSize);
        byte[] fileContent = FileToByte(fileObj);
        dataOutputStream.write(fileContent);
        System.out.println("Message sent!");
        String validation = "";
        validation = dataInputStream.readUTF();
        while (!validation.equals("Done"))
        {
            dataOutputStream.write(fileContent);
            validation = dataInputStream.readUTF();
        }

        dataInputStream.close();
        dataOutputStream.close();
        socket.close();
    }

    private static byte[] FileToByte(File f) throws IOException
    {
        FileInputStream fin = null;
        fin = new FileInputStream(f);

        byte fileContent[] = new byte[(int)f.length()];
        fin.read(fileContent);
        fin.close();
        return fileContent;
    }
}
